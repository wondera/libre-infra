# Service Management

# Sommaire

- [Service Management](#service-management)
- [Sommaire](#sommaire)
- [I. Simple services](#i-simple-services)
  - [1. A simple service](#1-a-simple-service)
  - [2. Logs](#2-logs)
- [II. Get dependant](#ii-get-dependant)
  - [1. Socket-based activation](#1-socket-based-activation)
  - [2. Mount and Automount](#2-mount-and-automount)
  - [3. Relations de dépendances](#3-relations-de-dépendances)
- [III. Service templates](#iii-service-templates)

# I. Simple services

## 1. A simple service

➜ **Créez un service simple**

- vous le créerez dans `/etc/systemd/system/`
- inspirez-vous des fichiers de *service* déjà existants sur la machine
- il doit exécuter la commande `python3 -m http.server 8888`, cela permet de lancer un mini-serveur web sur le port 8888 de la machine
- la commande doit être exécutée depuis `/var/www/html/test/` que vous aurez préalablement créé
- le processus doit s'exécuter en tant qu'un utilisateur `web` que vous aurez préalablement créé

Le strict minimum pour une unité de *service* fonctionnelle :

```systemd
[Unit]
Description=your custom description

[Service]
ExecStart=<COMMAND>
```

## 2. Logs

On va manipuler le démon *journald*.

➜ **La conf se trouve dans `/etc/systemd/journald.conf`**

- les options les plus intéressantes pour la conf concernent le stockage des logs :
  - la taille du fichier binaire
  - la fréquence de rotation des logs
  - l'endroit où est stocké le journal (sur disque, en RAM)

➜ **Utilisation usuelle de la commande `journalctl`**

- on peut consulter tous les logs du système avec `journalctl -xe`
  - paginé avec la commande `less`, on peut se balader avec les flèches du clavier, et quitter avec `q` (comme `quit`)
  - trié du plus vieux au plus récent
- attention, pour visualiser certains logs, il faudra être un utilisateur privilégié
  - on utilise souvent `journalctl` avec `sudo`
- on peut affiner la commande pour en tirer des choses très utiles :

```bash
# afficher les logs d'une unité spécifique
$ journalctl -xe -u <UNIT>
$ journalctl -xe -u httpd.service

# afficher les logs en temps réel
$ journalctl -xe -f

# afficher que les 20 dernières lignes
$ journalctl -xe -n 20

# désactiver le pager
$ journalctl -xe --no-pager

# transformer l'output de logs en d'autres formats
$ journalctl -xe --output=json
$ journalctl -xe --output=json-pretty
$ journalctl -xe --output=short-iso # ISO 8601 timestamp

# logs kernel
$ journalctl -xe -k

# afficher les logs d'un moment précis
$ journalctl --since=2022-03-15
$ journalctl --until=2022-03-15
# en commançant par les logs les plus récents
$ journalctl -S yesterday -U="2022-03-17 10:30:00" --reverse
```

# II. Get dependant

## 1. Socket-based activation

Le terme *socket-based activation* désigne le démarrage d'une *unité* lorsqu'un *socket* reçoit une requête. On va pouvoir démarrer un service Web à la demande par exemple.

Il est nécessaire que l'application le supporte. C'est le cas de beaucoup d'applications modernes, nous allons le tester avec NGINX.

➜ **Installer NGINX sur la machine**

- assurez-vous qu'il existe un service `nginx`
- assurez-vous qu'il est fonctionnel et que vous pouvez le démarrer

➜ **Socket activation**

- créer le fichier `/etc/systemd/system/nginx.service`
  - une copie du service original
  - vous pouvez utiliser `systemctl cat nginx` pour afficher le contenu du service original
  - ajouter une ligne `Environment=NGINX=3:4` dans la section `[Service]`
- créer le fichier `/etc/systemd/system/nginx.socket` muni du contenu suivant :

```systemd
[Unit]
Description=NGINX port
PartOf=nginx.service

[Socket]
ListenStream=0.0.0.0:80

[Install]
WantedBy = sockets.target
```

- vous pourrez ensuite mettre en place la *socket activation* :

```bash
# On démarre le service et on active le socket
$ sudo systemctl stop nginx
$ sudo systemctl start nginx.socket

# Le service a du être démarré automatiquement
$ curl localhost
```

## 2. Mount and Automount

Une unité de type *mount* permet de monter une partition. Une unité de type *automount* permet de monter une partition uniquement si elle est sollicitée (très utilisé pour les partition réseau)

➜ **Ajouter un disque à votre machine virtuelle**

```bash
# Partitionner le nouveau disque
$ sudo fdisk /dev/<DISK>

# Formater la partition
$ sudo mkfs.ext4 /dev/<PART>
```

➜ **Créer une unité de type *mount* et une *automount***

- syntaxe *mount* : `/etc/systemd/system/<UNIT>.mount`

```systemd
[Unit]
Description=foo mount

[Mount]
Where=/mnt/foo
What=/home/user/foo
Type=ext4
```

- syntaxe *automount* : `/etc/systemd/system/<UNIT>.automount`

```bash
[Unit]
Description=foo automount

[Automount]
Where=/mnt/foo

[Install]
WantedBy=multi-user.target
```

➜ **Une fois que tout est en place, tester l'*automount***

- disque ajouté, partition formatée, prête à être monté
- fichiers *mount* et *automount* en place
- démarrage de l'*automount*
  - un `sudo systemctl start <UNIT>.automount`
- vérifier que la partition est automatiquement montée lorsque vous l'interrogez avec un `ls` par exemple

## 3. Relations de dépendances

*systemd* intègre une gestion avancée des dépendances entre unités, avec des mots clé comme `After`, `Require`, `PartOf`, etc.

**La liste et la définition de ces relations de dépendances est disponible dans [la section dédiée de la doc officielle](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#Mapping%20of%20unit%20properties%20to%20their%20inverses).**

# III. Service templates

Il est possible de créer des *services* qui ne seront pas directement lancés, mais instanciés à chaque lancement. *systemd* utilise lui-même cette fonctionnalité pour gérer les sessions utilisateur.

> Ces unités ont un nom qui se termine par `@`. Il est aussi possible d'utiliser certaines variables dans le corps de l'unité, qui dépendent de l'instance lancée, comme son nom.

➜ **Tester l'unité suivante**, créez la dans `/etc/systemd/system/web@.service` :

```systemd
[Unit]
Description=Web server on port %i
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
User=root
ExecStart=/usr/bin/python3 -m http.server %i

[Install]
WantedBy=multi-user.target
```

Vous pouvez alors utiliser les commandes suivantes :

```bash
$ sudo systemctl start web@7000
$ sudo systemctl start web@7001
...
```

➜ **Plusieurs arguments**

Pour passer plusieurs arguments à la commande, il est nécessaire d'utilise la mécanique des variables d'environnement.

- créer deux dossiers
  - `/srv/web1/`
  - `/srv/web2/`
- créer deux fichiers
  - `/srv/web1/.env` qui contient `PORT=8888`
  - `/srv/web2/.env` qui contient `PORT=9999`
- utiliser l'unité suivante :

```systemd
[Unit]
Description=Web server %i
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
User=root
EnvironmentFile=/srv/%i/.env
ExecStart=/usr/bin/python3 -m http.server $PORT

[Install]
WantedBy=multi-user.target
```
