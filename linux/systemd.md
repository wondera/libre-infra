# systemd

***systemd* est une application qui occupe une place prépondérante des les OS GNU/Linux.** A peu de choses près, un OS GNU/Linux c'est le noyau Linux + systemd + quelques commandes GNU.

**Connaître *systemd*, c'est maîtriser l'OS.**  
C'est maîtriser l'OS pour le tune, pour l'adapter et l'affiner, pour mieux provisionner des machines et les configurer.  
C'est maîtriser l'OS pour la culture sur l'OS le plus utilisé au monde, pour mieux le hack et le retourner.  
C'est pouvoir pousser l'OS au maximum de ce qu'il peut nous apporter.

![Brace Yourselves](../images/brace-yourself-systemd.jpg)

# Sommaire

- [systemd](#systemd)
- [Sommaire](#sommaire)
- [I. What is systemd](#i-what-is-systemd)
  - [1. PID 1](#1-pid-1)
  - [2. Service manager](#2-service-manager)
  - [3. System manager](#3-system-manager)
    - [Exemple concret](#exemple-concret)
  - [4. Totally integrated to Linux](#4-totally-integrated-to-linux)
    - [A. New shiet](#a-new-shiet)
    - [B. But old shiet too](#b-but-old-shiet-too)
- [II. System manager](#ii-system-manager)
  - [1. Sessions utilisateur](#1-sessions-utilisateur)
  - [2. Partitions](#2-partitions)
  - [3. Time](#3-time)
  - [4. Logs](#4-logs)
  - [5. Tâches planifiées](#5-tâches-planifiées)
- [III. Unités systemd](#iii-unités-systemd)
  - [1. C koa une unité systemd](#1-c-koa-une-unité-systemd)
  - [2. Types d'unité](#2-types-dunité)
    - [A. Service](#a-service)
    - [B. Slice](#b-slice)
    - [C. Mount](#c-mount)
  - [D. Socket](#d-socket)
  - [3. Relations de dépendances](#3-relations-de-dépendances)

# I. What is systemd

*systemd* est né en 2010 et a peu à peu été adopté par toutes les plus distributions majeures. On peut citer par exemple :

- Debian et dérivés (Ubuntu, etc.)
- RedHat et dérivés (CentOS, Rocky, Fedora, etc.)
- ArchLinux et dérivés (Manjaro, etc.)

**Comprendre *systemd* c'est comprendre le fonctionnement d'un OS GNU/Linux.**

> On parle bien ici de l'OS et pas du noyau : *systemd* regroupe les fonctionnalités essentielles d'un OS.

*systemd* occupe trois fonctions au sein des OS GNU/Linux :

- PID 1
- system manager
  - gestion du temps
  - gestion du réseau
  - gestion de la langue
  - gestion de logs
  - etc.
- service manager
  - orchestre et programme des tâches
  - entretient et veille sur certains processus : les *services*

De ces 3 fonctions primaires découlent le fait que *systemd* occupe une place critique car il est responsable de la plupart des fonctions critiques de l'OS.

Les forces de *systemd* :

- complètement intégré à Linux
  - use et abuse de mécanismes natifs du noyau Linux
- propose des fonctionnalités avancées pour la gestion des processus
  - perfs, sécu, stabilité
- propose un format de fichier standard et unifié
  - c'est le concept d'*unité systemd*
  - une carte réseau est configurée avec la même syntaxe qu'un point de montage ou un *service*
- propose un écosystème cohérent pour gérer l'OS
  - réseau, gestion du temps, des logs, du DNS, etc.

***systemd* est composé aujourd'hui d'une myriade de programmes, chacun étant dédié à une tâche spécifique.**

## 1. PID 1

![Killing a child process](../images/killing-a-child-process.jpg)

*systemd* est le premier processus lancé lors du boot, une fois que le kernel a été chargé : on l'appelle le PID 1.

Le PID 1 a plusieurs rôles :

- lancer d'autres processus, qui, ensemble, constitueront l'OS
- il est le père de tous les autres processus
- il adopte les orphelins

*systemd* est connu notamment pour avoir accéléré drastiquement la vitesse de boot des systèmes GNU/Linux communs, notamment avec :

- une parallélisation "agressive" des tâches lors du démarrage de l'OS
- création de *socket* autonomes et *socket-based* activation

## 2. Service manager

Le rôle de *systemd* auquel on a très vite affaire dans tout système GNU/Linux aujourd'hui : taper des commandes `systemctl`.

*systemd* a pour rôle de veiller sur certains processus appelés *services*. Par "veiller", on entend notamment :

- créer un environnement d'exécution propice pour l'exécution du *service*
  - on peut définir par exemple :
    - un utilisateur qui lancera le process
    - un dossier depuis lequel le process s'exécutera
    - un *umask* spécifique pour le service
    - etc.
- redémarrer le *service* en cas de problème
- récolter les logs du *service*
- proposer des relations de dépendances avec d'autres *services* ou *unités systemd*

Comme mentionné dans la dernière ligne au dessus, *systemd* introduit la notion d'*unité systemd*. Les *services* ne sont qu'une des facettes des *unité* ; nombre d'autres *unités* sont disponibles afin de proposer un système cohérent. [+ d'infos dans la section dédiée](#1-c-koa-une-unité-systemd).

**Plus qu'un *service manager* on pourrait l'appeler, selon ses propres termes, un *unit manager*.**

![Systemd](../images/systemd.jpg)

## 3. System manager

> *Le terme de "system manager" il vient de moi, inutile de le Google mew.*

Enfin, la dernière place que *systemd* grignote petit à petit est celle de *system manager*.

---

> *Allez, une petite histoire ?*

**Un OS est constitué de plusieurs programmes**, chacun s'occupant d'une seule chose, afin de proposer à l'utilisateur un environnement cohérent dans lequel exécuter ses processus.

Avant *systemd*, ces programmes qui étaient les constituants des OS GNU/Linux étaient tous des projets complètement indépendants. Non pas que leur fonctionnement était mauvais (bien au contraire) mais il en résultait un OS aux multiples logiques, chaque programme ayant un fonctionnement qui lui était propre.

***systemd* essaie d'intégrer les fonctions occupés historiquement par ces outils indépendants et autonomes, afin de les rendre cohérentes entre elles.** Ainsi, *systemd* est le PID1 mais il comprend aussi beaucoup d'autres fonctions, que les OS GNU/Linux adopte petit à petit. Par exemple :

- *service manager*
  - remplace les scripts *sysV*, ou encore *upstart*
- reconnaissance des périphériques
  - intègre `udev`
- gestion des partitions
  - remplace `fstab`
- gestion des tâches planifiées
  - remplace `cron`
- gestion de la résolution de noms
  - avec un daemon DNS local
- gestion de la langue et de la locale
  - avec `localectl`
- gestion du temps
  - avec `systemd-timesyncd`

![Hungry systemd](../images/hungry-systemd.gif)

### Exemple concret

Un exemple concret. Avec *systemd* il devient très simple, *via* une syntaxe unifiée de :

- lancer un processus à intervalles réguliers (création d'un *service* et d'un *timer*)
- ce processus évolue dans un environnement restreint (quelques lignes dans la définition du service)
- s'il sollicite un point de montage, celui-ci est monté automatiquement au moment où il est accédé (création d'un *mount* et un *automount*)
- on garde une trace de toutes les exécutions de ce processus (grâce à *journald* système de logs centralisés, nativement lié au *service manager*)
  - on garde les logs du process
  - mais aussi une trace de tous ses arrêts, redémarrages, etc
- on garde une trace de tous ses comportements suspects (logs SELinux corrélés nativement à ceux de l'application)

**On fait avec une cinquantaine de lignes dans un syntaxe standard ce qu'on faisait avant avec un amoncellement de scripts shell de plusieurs centaines de lignes. Et c'est clair, transparent, maintenable, secure.**

## 4. Totally integrated to Linux

### A. New shiet

L'une des forces de *systemd* est d'apporter au premier plan certaines technologies modernes du noyau, permettant une meilleure gestion du système, un meilleur contrôle. Quelques exemples :

➜ **D-Bus**

- communication inter-process basé sur un système d'évènements et d'abonnement
- possibilité de monitorer cet espace
- possibilité d'interroger facilement *via* des libs

➜ ***namespaces***

- isolation de processus
- l'isolation est granulaire
  - on peut isoler seulement la stack réseau d'un processus par exemple
  - ou son arborescence de processus
- l'isolation est forte
  - mise en place par le noyau
  - l'isolation est native et obligatoire : mlise en place dès le boot par tout OS GNU/Linux

➜ ***CGroups***

- restriction d'accès aux ressources des processus
  - restriction d'accès au CPU, à la RAM, au réseau, etc.
  - impose des quotas
- monitoring de l'accès aux ressources par groupe et par processus
- complètement natif avec *systemd*, et personnalisable
  - **TOUS** les processus d'un GNU/Linux modernes sont déjà gérés de cette façon
  - ils sont placés dans des groupes, et leur accès aux ressources est restreint et monitorable

➜ **SELinux**

- mécanisme du noyau permettant de mettre en place une sécurité très granulaire au sein d'un OS GNU/Linux
- fastidieux à configurer mais extrêmement puissant
- présent nativement sur les systèmes dérivés de RedHat (CentOS, Rocky, Fedora, etc.)

![Everytime you disable SELinux, a kitten dies](../images/a-kitten-dies.jpg)

### B. But old shiet too

Dans le monde Linux, on aime bien les choses simples, qui fonctionnent bien, et qui fonctionnent avec les outils communs.

Certains mécanismes comme les *PID Files* ou les *Lock Files* sont aussi intégrés et gérés par *systemd*.

Il est ainsi aisé de précisé le *PID File* d'un *service* donné, afin que *systemd* s'en serve comme il se doit.

# II. System manager

## 1. Sessions utilisateur

*systemd* est responsable des sessions utilisateur, avec `systemd-logind`. Ses fonctions :

- c'est lui qui vous demande de saisir le nom d'un utilisateur et son mot de passe à la connexion
- entretient la liste des utilisateurs connectés
- trace et enregistre les connexions des utilisateurs

## 2. Partitions

Sur un système moderne, le fichier `/etc/fstab` est toujours utilisé : il est automatiquement converti par *systemd* à chaque boot, en unité de type *mount*.

En effet, *systemd* permet le montage de partitions grâce aux *unité* de type *mount*. Ayant une place privilégiée lors de la séquence de boot, a été fait le choix que *systemd* s'occuperait du montage des partitions au moment du boot.

Exemple dans un shell de l'unité de type *mount* automatiquement générée par *systemd*. C'est pour le montage d'une partition root `/`.

```bash
$ systemctl cat -- -.mount
# /run/systemd/generator/-.mount
# Automatically generated by systemd-fstab-generator

[Unit]
Documentation=man:fstab(5) man:systemd-fstab-generator(8)
SourcePath=/etc/fstab
Before=local-fs.target
After=blockdev@dev-disk-by\x2duuid-ba0f1de7\x2d1c05\x2d4a56\x2da824\x2d3801b81f80a5.target

[Mount]
What=/dev/disk/by-uuid/ba0f1de7-1c05-4a56-a824-3801b81f80a5
Where=/
Type=ext4
Options=rw,relatime
```

## 3. Time

*systemd* est responsable de la gestion de l'heure dans le système. On peut l'interroger et le manipuler avec la commande `timedatectl`.

Le daemon responsable de la gestion du temps s'appelle `systemd-timesyncd`.

## 4. Logs

*systemd* embarque un outil de journalisation : *journald*

*journald* a pour but de :

- récolter les logs de toutes les *unités systemd*
- récolter d'autres logs, comme les logs du kernel
- centraliser ces logs dans un fichier binaire
- mettre à disposition des admins des outils pour interagir avec ce fichier binaire, principalement la commande `journalctl`

> On aperçoit aussi les dernières lignes de logs d'une *unité* en bas des commandes `systemctl status <UNIT>`.

Les logs sont stockés dans un binaire. Cela veut dire qu'il n'est pas conseillé de les lire directement, mais uniquement avec de soutils dédiés. Cela permet aussi de pouvoir facilement convertir les logs dans n'importe quel format : texte, JSON, autres.

Vous pouvez penser à *journald* comme une mini stack Elastic, en locale, sur toutes les machines Linux du monde, et `journalctl` est l'outil pour query.

## 5. Tâches planifiées

*systemd* met en place les *timer* qui permettent l'exécution de tâches à intervalles réguliers. Cela remplace l'utilisation de `cron`.

Les *timer* proposent des fonctionnalités très avancés par rapport à `cron` et s'intègre nativement avec tout le reste de la stack *systemd*.

**Ainsi, un *timer* doit forcément lancer un *service*.**  
Contrainte ?  
C'est une contrainte si pour vous des choses comme avoir automatiquement des logs ou de la sécurité *via* l'isolation des process ou encore avoir une traçabilité de toutes les exécutions du processus, sont des contraintes. :)

![What happened](../images/what-happened-to-my-cron-job.jpg)

# III. Unités systemd

## 1. C koa une unité systemd

Les *unités systemd* ou *systemd units* sont des éléments que *systemd* gère. Une *unité* c'est une représentation d'une ressource du système que *systemd* peut gérer.

Okok soyons tout de suite concrets. Voici les différentes ressources que peut gérer *systemd* :

- processus avec les unités *service* et *scope*
- groupes de processus avec les *slices*
- points de montage avec les *mount* et *automount*
- tâche planifiée avec *timer*
- chemin dans l'arborescence de fichier avec *path*
- socket avec... *socket* hihi
- un périphérique avec *device*
- une carte réseau avec *network* (uniquement si le système utilise `systemd-networkd`)
- un groupe d'autres unités avec *target*

---

**Une *unité systemd*, quelque soit son type, est défini avec une syntaxe unifiée et standard.**

Pour créer une nouvelle unité, il suffit de créer un fichier dans l'un des *paths* spécifiques gérés par *systemd*.

> Le path à utiliser en tant qu'admin est `/etc/systemd/system/`. Il existe d'autres paths possibles, réservés à d'autres usages. La plupart des *unités* installées par des paquets, par exemple, iront dans `/usr/lib/systemd/system/`.

La syntaxe de ces fichiers d'*unité* est standard, peu importe le type d'unité et l'OS spécifique.
Par convention, le nom de l'*unité* doit être unique, et l'extension du fichier est le type de l'*unité*.

> On a par exemple `apache.service` pour une unité "apache" qui est de type *service*. Ou encore par exemple `backup.path` pour une unité "backup" de type *path*.  
Lors de l'utilisation de commandes `systemctl`, si aucun type n'est précisé, alors le type *service* est utilisé par défaut.

## 2. Types d'unité

### A. Service

Un *service*, pour *systemd* c'est un processus dont il aura la charge de prendre soin, pendant tout son cycle de vie. Cela implique :

- gérer l'environnement dans lequel évolue le processus
- gérer le redémarrage automatique du *service*
- gérer les dépendances des *services* entre eux
- gérer les logs du *services*
- proposer à l'admin des outils pour interagir avec les *services*

*systemd* va proposer nativement certaines fonctionnalités avancées pour l'environnement d'exécution du processus, quelques exemples :

- utilisation d'un utilisateur spécifique
- création de point de montages temporaires pour `/tmp` ou `/home` afin de les isoler du reste du système
- isolation du processus dans des namespaces spécifiques
- restriction d'accès aux ressources à l'aide des CGroupes
- corrélation native des logs avec le reste du système grâce à la gestion de logs centralisée
- *socket-based activation*
- gestion de PID File

### B. Slice

*systemd* utilise les *CGroups* nativement.

Les *CGroups* permettent de :

- réunir les processus en groupes
- appliquer à ces groupes des restrictions d'accès aux ressources
- monitorer l'accès aux ressources de ces groupes

***systemd* utilise crée par défaut plusieurs arborescences CGroups, on peut les manipuler avec une unité de type *slice*.**

---

**Un *slice* est utilisé pour grouper des ressources et des processus, des *scopes* et des *services*** (ce sont les deux types d'unités capables d'exécuter des process).

Il existe un *root slice*, noté `-.slice`. Tous les autres *slices* sont des enfants du *root slice*.

Sur la plupart des systèmes GNU/Linux modernes, *systemd* met en place par défaut 3 *slices* :

- le *system.slice*
  - les *service* et les *scope* sont placés dans ce *slice*
- le *machine.slice*
  - les conteneurs et les VMs gérés par *systemd* sont placés dans ce *slice*
- le *user.slice*
  - les sessions des utilisateurs sont placées dans ce dernier *slice*

---

Il est possible de :

- créer de nouveaux *slices*
  - simplement enfants du *root slice* `-.slice`
  - ou enfant d'un *slice* existant
  - simplement en créant un fichier qui se termine par `.slice`, syntaxe standard des unités !
- appliquer des restrictions d'accès aux ressources sur chacun de ces slices
- accéder à un monitoring en temps réel des slices avec `systemd-cgls` et `systemd-cgtop`

### C. Mount

Les targets *mount* sont un remplacement de la mécanique `fstab`.

Sur la plupart des systèmes munis de *systemd*, le fichier `/etc/fstab` est géré par *systemd* et ce fichier est converti dans le formé standard de définition d'*unités*. Exemple de conversion automatique (le montage de la partition `/`) :

```bash
$ systemctl cat -- -.mount
# /run/systemd/generator/-.mount
# Automatically generated by systemd-fstab-generator

[Unit]
Documentation=man:fstab(5) man:systemd-fstab-generator(8)
SourcePath=/etc/fstab
Before=local-fs.target
After=blockdev@dev-disk-by\x2duuid-ba0f1de7\x2d1c05\x2d4a56\x2da824\x2d3801b81f80a5.target

[Mount]
What=/dev/disk/by-uuid/ba0f1de7-1c05-4a56-a824-3801b81f80a5
Where=/
Type=ext4
Options=rw,relatime
```

## D. Socket

Les *unités socket* permettent de définir l'emplacement d'un *socket* de plusieurs types. Cela est principalement utilisé pour deux choses :

- parallélisation et dépendances du démarrage d'unités
  - certaines applis ont besoin qu'on *socket* existe pour pouvoir démarrer
  - mais elles n'ont pas nécessairement besoin qu'un programme soit derrière tout de suite pour répondre à leurs requêtes
  - *systemd* permet de créer le *socket* d'écoute AVANT de lancer une app derrière
- socket-based activation
  - *systemd* est capable de détecter des messages reçus sur le *socket* et ainsi déclencher le démarrage d'un *service* qui pourra alors lire le *socket* et traiter la requête

Exemple concret : on peut ouvrir un port réseau TCP, sans lancer de programme derrière. Et uniquement quand un client fera une requête, alors on exécutera le *service*.

## 3. Relations de dépendances

Et c'est là que ça devient un gros banger comme on dit dans le jargon.

***systemd* intègre une gestion de dépendances entre toutes ces unités.** Le système de dépendance est extrêmement fin et granulaire, ne se contentant pas de simple "after" et "before" pour ordonner les tâches.

---

Le système de relations permet de faire des choses fines comme :

- démarrer une unité automatiquement après qu'une autre ait été démarrée
- éteindre une unité automatiquement si une autre est éteinte
- indiquer qu'une unité ne doit pas démarrer tant qu'une autre n'a pas atteint un certain état
- indiquer que plusieurs unités sont un groupe et doivent être démarrées ensemble
- indiquer que certaines unités sont conflictuelles entre elles et ne doivent pas être lancées en même temps

Ce système de dépendance est utilisé de façon abusive lors du boot du système afin de paralléliser les tâches avec grande précision.

---

Il est possible de visualiser ces dépendances avec plusieurs commandes :

- `systemctl list-dependencies <UNIT>` permet de voir dans le terminal les dépendances d'une unité specifique
- `systemd-analyse dot <UNIT> | dot -Tsvg > file.svg` permet de générer un graphe au format SVG des dépendances d'une unité données
  - la commande supporte plusieurs unités
