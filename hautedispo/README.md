# Gestion de la disponibilité

- Les principes de la gestion de la [disponibilité](./disponibilite.md)

## Redondance sur serveur physique

- [Configuration réseaux redondante sous linux](./config-net-advanced.md) avancée sous linux
- [Le multipathing](./iscsi-mutlipath.md) sous linux

## Redonndance applicative

- Le couteau suisse réseaux [keepalived](./keepalived.md)
- La [réplication Mysql](./mysql-replication.md)
