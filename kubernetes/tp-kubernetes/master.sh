#!/bin/bash
test -f /root/kubeinstallmasterdone && echo "already installed"
test -f /root/kubeinstallmasterdone && exit 0

# init cluster
kubeadm init --apiserver-advertise-address=192.168.33.100 --pod-network-cidr=10.42.0.0/16 >> /root/kubeinit.log 2>/dev/null

# give vagrant access
mkdir /home/vagrant/.kube /root/.kube
cp /etc/kubernetes/admin.conf /home/vagrant/.kube/config
cp /etc/kubernetes/admin.conf /root/.kube/config
chown -R vagrant:vagrant /home/vagrant/.kube

# weavenet CNI
curl -s -L "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" -o weavenet.yml
kubectl apply -f weavenet.yml

kubeadm token create --print-join-command > /home/vagrant/joinkubecluster.sh
chown vagrant:vagrant /home/vagrant/joinkubecluster.sh

# serveur NFS
mkdir /opt/data
apt-get install -qq -y nfs-kernel-server
cat >> /etc/exports <<EOF
/opt/data 192.168.33.0/24(rw,no_root_squash)
EOF
service nfs-kernel-server reload

touch /root/kubeinstallmasterdone
