# Loadbalancing

* [MetalLB](#metallb)
  * [Déploiement de metalLB](#deploiement-de-metallb)
  * [Déploiement d'un ReplicatSet derrière le loadbalanceur](#d-ploiement-d-un-ReplicatSet-derri-re-le-loadbalanceur)
* [Conclusion](#conclusion)

## MetalLB

### Configuration

Création d'un namespace dédié **metallb-system** et déploiement d'une ConfigMap pour la configuration de metal LB

```bash
$ kubectl apply -f metallb-config.yml 
configmap/config created
$ kubectl describe configmap config -n metallb-system
Name:         config
Namespace:    metallb-system
Labels:       <none>
Annotations:  <none>

Data
====
config:
----
address-pools:
- name: default
  protocol: layer2
  addresses:
  - 192.168.33.10-192.168.33.10
- name: realpool
  protocol: layer2
  addresses:
  - 192.168.33.20-192.168.33.30


BinaryData
====

Events:  <none>
```

Nous avons défini deux adresse pool le `default` qui ne contien qu'une ip `192.168.33.10` et le `realpool` avec 11 adresses : `192.168.33.20-192.168.33.30`

### Déploiement

Déploiement :

* Les ServiceAccount controller et speaker
* Des Role/ClusterRole et RoleBindings
* Un DaemonSet daemonset.apps/speaker
* Un déploiement deployment.apps/controller

```bash
$ kubectl apply -f metallb.yml
namespace/metallb-system created
podsecuritypolicy.policy/controller created
podsecuritypolicy.policy/speaker created
serviceaccount/controller created
serviceaccount/speaker created
clusterrole.rbac.authorization.k8s.io/metallb-system:controller created
clusterrole.rbac.authorization.k8s.io/metallb-system:speaker created
role.rbac.authorization.k8s.io/config-watcher created
role.rbac.authorization.k8s.io/pod-lister created
role.rbac.authorization.k8s.io/controller created
clusterrolebinding.rbac.authorization.k8s.io/metallb-system:controller created
clusterrolebinding.rbac.authorization.k8s.io/metallb-system:speaker created
rolebinding.rbac.authorization.k8s.io/config-watcher created
rolebinding.rbac.authorization.k8s.io/pod-lister created
rolebinding.rbac.authorization.k8s.io/controller created
daemonset.apps/speaker created
deployment.apps/controller created
```

Resultat :

```bash
$ kubectl get all -n metallb-system
NAME                             READY   STATUS    RESTARTS   AGE
pod/controller-57fd9c5bb-7bctk   1/1     Running   0          7m25s
pod/speaker-89zsj                1/1     Running   0          7m25s
pod/speaker-hvjf9                1/1     Running   0          7m25s

NAME                     DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/speaker   2         2         2       2            2           kubernetes.io/os=linux   7m25s

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/controller   1/1     1            1           7m25s

NAME                                   DESIRED   CURRENT   READY   AGE
replicaset.apps/controller-57fd9c5bb   1         1         1       7m25s

$ kubectl describe configmap config -n metallb-system
Name:         config
Namespace:    metallb-system
Labels:       <none>
Annotations:  <none>

Data
====
config:
----
address-pools:
- name: default
  protocol: layer2
  addresses:
  - 192.168.33.10-192.168.33.10
- name: realpool
  protocol: layer2
  addresses:
  - 192.168.33.20-192.168.33.30


BinaryData
====

Events:  <none>

```

### Déploiement d'un ReplicatSet derrière le loadbalanceur

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: tst-pod-nginx-config
data:
  default.conf: |
    server {
        listen 80;

        location / {
            default_type text/plain;
            expires -1;
            return 200 'Server address: $server_addr:$server_port\nServer name: $hostname\nDate: $time_local\nURI: $request_uri\nRequest ID: $request_id\n';
        }
    }
---
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  labels:
    run: tst-ct-nginx
  name: tst-replicaset
spec:
  replicas: 2
  selector:
    matchLabels:
      applicat: tst-pod-label-nginx
  template:
    metadata:
      labels:
        applicat: tst-pod-label-nginx
    spec:
      containers:
      - image: nginx
        name: tst-ct-nginx
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - name: nginx-configs
            mountPath: /etc/nginx/conf.d
      volumes:
        - name: nginx-configs
          configMap:
            name: tst-pod-nginx-config
---
apiVersion: v1
kind: Service
metadata:
  labels:
    name: tst-svc-label-nginx
  annotations:
    metallb.universe.tf/address-pool: realpool          # definition du pool
    metallb.universe.tf/loadBalancerIPs: 192.168.33.30  # et de l'ip
  name: tst-svc-nginx
spec:
  type: LoadBalancer                                    # appel au loadbalanceur du cluster
  ports:
    - port: 1080
      targetPort: 80
  selector:                                       # selection des templates de pods
    applicat: tst-pod-label-nginx
```

```bash
$ kubectl apply -f tst-replicatset-nginx-lb.yml
configmap/tst-pod-nginx-config created
replicaset.apps/tst-replicaset created
service/tst-svc-nginx created
$ kubectl get all
NAME                       READY   STATUS    RESTARTS   AGE
pod/tst-replicaset-868ql   1/1     Running   0          104s
pod/tst-replicaset-rv9l4   1/1     Running   0          104s

NAME                    TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)          AGE
service/kubernetes      ClusterIP      10.96.0.1     <none>          443/TCP          13d
service/tst-svc-nginx   LoadBalancer   10.99.39.25   192.168.33.30   1080:32594/TCP   104s

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/tst-replicaset   2         2         2       104s
```

On notera l'external IP et que le site est maintenant accessible via l'url
<http://192.168.33.30:1080/>

## Conclusion

Nous pouvons maintenant déployer des services réellement accessible sur une ip publique.

Pour ce faire nous definissons le service de type load balanceur:

```yaml
---
apiVersion: v1
kind: Service
metadata:
  annotations:
    metallb.universe.tf/address-pool: <POOL>
    metallb.universe.tf/loadBalancerIPs: <IPFROMPOOL>
  name: <SERVICENAME>
spec:
  type: LoadBalancer
  ports:
    - port: <PUBLICPORT>
      targetPort: <CONTAINERPORT>   # le port sur le pod
```

Dans notre exemple nous retrouvons bien le port public 1080, le port de kubeproxy sur le node : 32594 et enfin le port sur le container : 80

```bash
$ kubectl get services tst-svc-nginx
NAME            TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)          AGE
tst-svc-nginx   LoadBalancer   10.99.39.25   192.168.33.30   1080:32594/TCP   34m
$ kubectl get endpoints tst-svc-nginx
NAME            ENDPOINTS                   AGE
tst-svc-nginx   10.44.0.5:80,10.44.0.6:80   34m
```
