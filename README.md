# Plan de cours

## Présentation de la majeure

[Descriptif](./majeur/descriptif.md) du cours

- [Open source vs logiciel libre](./majeur/logiciel-libre.md)
- [Open source en france](./majeur/open-source-en-france.md)
- [Présentation du projet](./majeur/projet.md)

### Contexte

- [rappels GNU/linux](./linux/rappels-linux.md)
- Principes de [gestion d'une infrastructure](./majeur/gestion-infra-run.md)
- La [maturité des systèmes d'informations](./majeur/maturite-SI.md)
- Description d'une platforme de service : [le DAT](./majeur/description-infra.md)

## La gestion de conf avec Ansible

### preambule

- Présentation de [la gestion des configuration](./gestion-de-conf/presentation-gestion-conf.md)
- Pour ceux qui ne connaissent pas une présentation de l'outil [Vagrant](./gestion-de-conf/vagrant.md) qui est bien utile pour ce cours.

### Debuter avec Ansible

Pour ceux qui ne connaissent pas Ansible :

- Pour comprendre et utiliser ansible : [Présentation Ansible](./gestion-de-conf/presentation-ansible.md)
- Pour comprendre et [Ecrire du code Ansible](./gestion-de-conf/code-ansible.md)

### Un peu plus d'Ansible

- Quelques [pratiques Ansible](./gestion-de-conf/pratiques-ansible.md) permettant d'organiser son code ansible.
- Une petite [présentaiton de l'outil ansible molécule](./gestion-de-conf/ansible-molecule.md) permettant d'automatiser les tests des roles Ansible.

## La conteneurisation

- [Les containers](./linux/containers.md)

### TP conteneurisation

- [containeurs en profondeur](./linux/tp-containers/ct-in-depth/README.md)
- [outils lié à la conteneurisation](./linux/tp-containers/ct-tooling/README.md)
- [Soyez le plus rapide](./linux/tp-containers/be-the-fastest/README.md)

## L'orchestration de conteneur avec Kubernetes

### Préambule

- [Le-cloud](../kubernetes/presentation-cloud.md)
- [Présentaiton Kubernetes](../kubernetes/presentation-kube.md)

### TP-cours

Prendre ces notions par la pratique :

- [Construction d'un lab](./kubernetes/lab-kube.md)
- [Tests de déploiement d'application](./kubernetes/kube-deployments.md)
- [Gestion des volumes](./kubernetes/kube-volumes.md)
- [Création d'un Loadbalanceur MetalLB](./kubernetes/kube-metallb.md)
- [Utilisation de helm](./kubernetes/kube-helm-monitoring.md) pour monitorer votre cluster
- [Déploiement d'un Ingress](./kubernetes/kube-ingress.md)

## Advanced GNU/linux

- [systemd](./linux/systemd.md)
- [tp systemd](./linux/tp-systemd/README.md)
- [hardening du systeme](./linux/linux_os_hardening.md)
- [tp hardening](./linux/tp-hardening/README.md)

## Gestion de la disponibilité

- Les principes de la gestion de la [disponibilité](./hautedispo/disponibilite.md)

### Redondance sur serveur physique

- [Configuration réseaux redondante sous linux](./hautedispo/config-net-advanced.md) avancée sous linux
- [Le multipathing](./hautedispo/iscsi-mutlipath.md) sous linux

### Redonndance applicative

- Le couteau suisse réseaux [keepalived](./hautedispo/keepalived.md)
- La [réplication Mysql](./hautedispo/mysql-replication.md)
