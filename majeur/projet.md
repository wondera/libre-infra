# Présentation projet

## Cadre

Afin de valider la majeure. Vous devrez mettre en place en groupe un service ou une briques de service réutilisable basés sur une ou des solutions opensources.

Vous definissez un besoin fonctionnel et vous y réponder techniquement en intégrant un niveau de service garanti en terme de disponibilité et fiabilité.

Le service founi doit être bien défini et simple à gèrer, c'est la finalité de votre projet.

## Rendu

### Oral

Le dernier jour, tous les groupes présentent leur projet à l'ensemble des autres groupes.

La présentation Orale devra mettre en valeur le travail effectué et ce qu'il apporte.
